<?php

namespace Drupal\simple_affiliate\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class to provide functionality for SetTracking.
 *
 * @package Drupal\simple_affiliate\Controller
 */
class SetTracking extends ControllerBase {

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  private $requestStack;

  /**
   * Constructor of SetTracking.
   */
  public function __construct(RequestStack $requestStack) {
    $this->requestStack = $requestStack;
  }

  /**
   * Setting the tracking cookie.
   */
  public function setAffiliateTrackingCookie($uid) {
    $cookieValue = $this->requestStack->getCurrentRequest()->cookies->get('simple_affiliate');

    if ($cookieValue === NULL) {
      $response = new Response();
      $response->headers->setCookie(new Cookie('simple_affiliate', $uid, time() + 60 * 60 * 24 * 180, '/'));
      $response->send();
    }

    return new RedirectResponse('user.register');
  }

}
