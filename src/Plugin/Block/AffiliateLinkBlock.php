<?php

namespace Drupal\simple_affiliate\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides automatically generated affiliate link with user id.
 *
 * @Block(
 *   id = "simple_affiliate_referral_link",
 *   admin_label = @Translation("Simple Affiliate Referral Link Block"),
 * )
 */
class AffiliateLinkBlock extends BlockBase {

  /**
   * The current user account.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * Constructs an AffiliateLinkBlock object.
   *
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user account.
   */
  public function __construct(AccountInterface $current_user) {
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('current_user')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {

    // Getting the current user ID.
    $user_id = $this->currentUser->id();

    // Generating the link from the route.
    $link = Url::fromRoute('simple_affiliate.settracking');
    $link = $link->toString();
    $host = $this->requestStack->getCurrentRequest()->getSchemeAndHttpHost();
    return [
      '#markup' => $host . $link . '/' . $user_id,
      '#prefix' => '<link>',
      '#suffix' => '</link>',
      '#attributes' => [
        'style' => ['text-shadow: none'],
      ],
    ];
  }

}
